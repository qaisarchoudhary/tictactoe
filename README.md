# Getting Started

### Before we start:
* This code comprises of backend part only written in following technologies
* * Spring boot 2.5.0 [Restful framework]
* * postgresQL [Database]
* * JOOQ [As database entity framework, more can be found at https://www.jooq.org]
* * Websockets
* * Swagger API integration for generating RESTful API definition
* * Junit 5
* * flyway for database migrations 

### The API can be found at:
http://localhost/swagger-ui/

### Some assumptions and notes:
* Database is kept simple purposefully to serve needs of this tiny application
* Due to limited time, Unit and Integration tests are written only for controller class just to show my test 
  methodology, in practice whole the code should be covered by automated tests for better quality.
* All API endpoints are requiring ***userUuid*** as Header, but in practice this value will be taken from logged-in 
   user ***Principal***
* There is need to create secondary database indices

### Build the project
Please follow the following the steps to build the project 
#### Starting a postgress docker instance:
0. ***docker run --name postgres-instance -e POSTGRES_PASSWORD=devpwd -d -p 5432:5432 postgres***
#### Building the project:
1. ***./gradlew flywayMigrate*** [this will do database migration using flyway plugin]
2. ***./gradlew generateJooqSourceCode*** [this will create JOOQ classes reading the database]
3. ***./gradlew build*** [At this stage you will be able to build the application]

### Deployment
Please follow these step to deploy locally
#### Starting a postgress docker instance:
* Assuming postgres docker instance is already running as described above
#### Building the project:
1. ***java -jar build/libs/tictactoe-0.0.1-SNAPSHOT.jar*** [Run this in project home directory]
   You can of course choose to set following variable located in application.yaml according to those chosen when 
   started database.
   * ${DATASOURCE_URL}
   * ${DATASOURCE_USERNAME}
   * ${DATASOURCE_PASSWORD}

    By default, this application runs on port 80, you may in some cases run the ***"java -jar...."*** command with 
    ***sudo***



