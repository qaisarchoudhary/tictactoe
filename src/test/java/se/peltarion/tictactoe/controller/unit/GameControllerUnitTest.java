package se.peltarion.tictactoe.controller.unit;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import se.peltarion.tictactoe.controller.GameController;
import se.peltarion.tictactoe.exception.GameNotAvailableException;
import se.peltarion.tictactoe.exception.OtherPrayerMoveException;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.tictactoe.model.api.response.GameBoard;
import se.peltarion.tictactoe.model.game.GameStatus;
import se.peltarion.tictactoe.model.game.GameSymbol;
import se.peltarion.tictactoe.model.service.Move;
import se.peltarion.tictactoe.model.api.response.MoveResponse;
import se.peltarion.tictactoe.service.GameService;
import se.peltarion.tictactoe.testdata.TestGameDataProvider;
import se.peltarion.tictactoe.testdata.TestMoveDataProvider;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerUnitTest {

  private static final String controllerPath = "/tictactoe";

  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private SimpMessagingTemplate testSimpMessagingTemplate;

  @Mock
  private GameService gameService;

  @BeforeEach
  public void setUp() {
    this.mockMvc =
        MockMvcBuilders.standaloneSetup(new GameController(this.gameService, this.testSimpMessagingTemplate)).build();
  }

  @Test
  void testShouldReturnGameBoardOnGettingGameBoard() throws Exception {
    final String endpoint = controllerPath + "/get-game-board";

    final Game testGame = TestGameDataProvider.getSampleGame();
    final Move testMove =
        TestMoveDataProvider.getMoveByUserUuidAndPlayerUuid(testGame.getUuid(), testGame.getPlayerOneUuid());

    GameBoard tesGameBoard =
        GameBoard.Builder.game(testGame).moves(Collections.singletonList(testMove)).build();

    when(this.gameService.getGameBoard(testGame.getPlayerOneUuid(), testGame.getUuid())).thenReturn(tesGameBoard);

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(endpoint)
        .header("userUuid", testGame.getPlayerOneUuid().toString())
        .queryParam("gameUuid", testGame.getUuid().toString())
    ).andReturn();

    final GameBoard responseGameBoard = this.objectMapper
        .readValue(mvcResult
                .getResponse()
                .getContentAsString(),
            new TypeReference<GameBoard>() {
            });

    assertThat(responseGameBoard).isNotNull();
    assertThat(responseGameBoard.getGame().getUuid()).isEqualTo(testGame.getUuid());
    assertThat(responseGameBoard.getGame().getPlayerOneUuid()).isEqualTo(testGame.getPlayerOneUuid());
    assertThat(responseGameBoard.getGame().getStatus()).isEqualTo(testGame.getStatus());
    assertThat(responseGameBoard.getGame().getCreated()).isEqualTo(testGame.getCreated());

    assertThat(responseGameBoard.getMoves().get(0).getUuid()).isEqualTo(testMove.getUuid());
    assertThat(responseGameBoard.getMoves().get(0).getPlayerUuid()).isEqualTo(testMove.getPlayerUuid());
    assertThat(responseGameBoard.getMoves().get(0).getRow()).isEqualTo(testMove.getRow());
    assertThat(responseGameBoard.getMoves().get(0).getCol()).isEqualTo(testMove.getCol());
    assertThat(responseGameBoard.getMoves().get(0).getCreated()).isEqualTo(testMove.getCreated());

  }

  @Test
  void testShouldReturnAvailableGameListOnGettingAvailableGames() throws Exception {
    final String endpoint = controllerPath + "/get-available-games";

    final Game testGame = TestGameDataProvider.getSampleGame();

    when(this.gameService.getAvailableGames()).thenReturn(Collections.singletonList(testGame));

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(endpoint)
        .header("userUuid", UUID.randomUUID())
    ).andReturn();

    final List<Game> responseGames = this.objectMapper
        .readValue(mvcResult
                .getResponse()
                .getContentAsString(),
            new TypeReference<List<Game>>() {
            });

    assertThat(responseGames).isNotNull();
    assertThat(responseGames.size()).isPositive();
    assertThat(responseGames.get(0)).isNotNull();
    assertThat(responseGames.get(0).getUuid()).isEqualTo(testGame.getUuid());
    assertThat(responseGames.get(0).getPlayerOneUuid()).isEqualTo(testGame.getPlayerOneUuid());
    assertThat(responseGames.get(0).getStatus()).isEqualTo(testGame.getStatus());
    assertThat(responseGames.get(0).getCreated()).isEqualTo(testGame.getCreated());

  }

  @Test
  void testShouldReturnValidGameOnCreatingGame() throws Exception {
    final String endpoint = controllerPath + "/create-game";

    final Game testGame = TestGameDataProvider.getSampleGame();

    when(this.gameService.createGame(testGame.getPlayerOneUuid())).thenReturn(testGame);

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
        .header("userUuid", testGame.getPlayerOneUuid())
    ).andReturn();

    final Game responseGame = this.objectMapper
        .readValue(mvcResult
                .getResponse()
                .getContentAsString(),
            new TypeReference<Game>() {
            });

    assertThat(responseGame).isNotNull();
    assertThat(responseGame.getUuid()).isEqualTo(testGame.getUuid());
    assertThat(responseGame.getStatus()).isEqualTo(GameStatus.NOT_STARTED.getDescription());
    assertThat(responseGame.getWinner()).isNull();
    assertThat(responseGame.getPlayerOneUuid()).isEqualTo(testGame.getPlayerOneUuid());

  }


  @Test
  void testShouldReturnValidMoveOnTalkingMove() throws Exception {
    final String endpoint = controllerPath + "/move";

    Game testGame = TestGameDataProvider.getSampleGameWithPlayStatus();

    MoveResponse testMoveResponse =
        TestMoveDataProvider.getMoveResponseByUserUuidAndPlayerUuid(testGame.getUuid(), testGame.getPlayerOneUuid());

    when(this.gameService.move(testGame.getPlayerOneUuid(),
        testGame.getUuid(),
        testMoveResponse.getMove().getRow(),
        testMoveResponse.getMove().getCol()))
        .thenReturn(testMoveResponse);

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
        .header("userUuid", testGame.getPlayerOneUuid())
        .queryParam("gameUuid", testGame.getUuid().toString())
        .queryParam("row", String.valueOf(testMoveResponse.getMove().getRow()))
        .queryParam("col", String.valueOf(testMoveResponse.getMove().getCol()))
    ).andReturn();

    final MoveResponse responseMove = this.objectMapper
        .readValue(mvcResult
                .getResponse()
                .getContentAsString(),
            new TypeReference<MoveResponse>() {
            });

    assertThat(responseMove).isNotNull();
    assertThat(responseMove.getMove().getPlayerUuid()).isEqualTo(testGame.getPlayerOneUuid());
    assertThat(responseMove.getMove().getRow()).isEqualTo(testMoveResponse.getMove().getRow());
    assertThat(responseMove.getMove().getCol()).isEqualTo(testMoveResponse.getMove().getCol());

  }


  @Test
  void testShouldReturnValidGameOnJoiningGame() throws Exception {
    final String endpoint = controllerPath + "/join-game";

    final Game testGame = TestGameDataProvider.getSampleGameWithPlayStatus();

    final UUID playerTwoUuid = UUID.randomUUID();
    when(this.gameService.joinGame(playerTwoUuid, testGame.getUuid())).thenReturn(testGame);

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.put(endpoint)
        .header("userUuid", playerTwoUuid)
        .queryParam("gameUuid", testGame.getUuid().toString())
    ).andReturn();

    final Game responseGame = this.objectMapper
        .readValue(mvcResult
                .getResponse()
                .getContentAsString(),
            new TypeReference<Game>() {
            });

    assertThat(responseGame.getUuid()).isEqualTo(testGame.getUuid());
    assertThat(responseGame.getPlayerOneUuid()).isEqualTo(testGame.getPlayerOneUuid());
    assertThat(responseGame.getStatus()).isEqualTo(GameStatus.PLAY.getDescription());
    assertThat(responseGame.getCreated()).isEqualTo(testGame.getCreated());
    assertThat(responseGame.getPlayerOneSymbol()).isEqualTo(GameSymbol.PLAYER_ONE.getSymbol());
    assertThat(responseGame.getPlayerTwoSymbol()).isEqualTo(GameSymbol.PLAYER_TWO.getSymbol());

  }


  @Test
  void testShouldReturnGameNotAvailableExceptionOnGettingGameBoard() throws Exception {
    final String endpoint = controllerPath + "/get-game-board";

    final UUID userUuid = UUID.randomUUID();
    final UUID gameUuid = UUID.randomUUID();
    when(this.gameService.getGameBoard(userUuid, gameUuid)).thenThrow(new GameNotAvailableException());

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(endpoint)
        .header("userUuid", userUuid.toString())
        .queryParam("gameUuid", gameUuid.toString())
    ).andReturn();

    assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    assertThat(Objects.requireNonNull(mvcResult.getResolvedException())
        .getMessage()).isEqualTo(new GameNotAvailableException().getMessage());

  }

  @Test
  void testShouldReturnGameNotAvailableExceptionOnTakingMove() throws Exception {
    final String endpoint = controllerPath + "/move";

    final UUID userUuid = UUID.randomUUID();
    final UUID gameUuid = UUID.randomUUID();

    when(this.gameService.move(userUuid, gameUuid, 0, 0)).thenThrow(new GameNotAvailableException());

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
        .header("userUuid", userUuid.toString())
        .queryParam("gameUuid", gameUuid.toString())
        .queryParam("row", String.valueOf(0))
        .queryParam("col", String.valueOf(0))
    ).andReturn();

    assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    assertThat(Objects.requireNonNull(mvcResult.getResolvedException())
        .getMessage()).isEqualTo(new GameNotAvailableException().getMessage());

  }

  @Test
  void testShouldReturnOtherPrayerMoveExceptionOnTakingMove() throws Exception {
    final String endpoint = controllerPath + "/move";

    final UUID userUuid = UUID.randomUUID();
    final UUID gameUuid = UUID.randomUUID();

    when(this.gameService.move(userUuid, gameUuid, 0, 0)).thenThrow(new OtherPrayerMoveException());

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
        .header("userUuid", userUuid.toString())
        .queryParam("gameUuid", gameUuid.toString())
        .queryParam("row", String.valueOf(0))
        .queryParam("col", String.valueOf(0))
    ).andReturn();

    assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
    assertThat(Objects.requireNonNull(mvcResult.getResolvedException())
        .getMessage()).isEqualTo(new OtherPrayerMoveException().getMessage());

  }
}

