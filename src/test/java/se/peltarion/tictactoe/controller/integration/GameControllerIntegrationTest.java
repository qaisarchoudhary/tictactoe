package se.peltarion.tictactoe.controller.integration;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.jooq.DSLContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import se.peltarion.tictactoe.controller.GameController;
import se.peltarion.tictactoe.exception.GameNotAvailableException;
import se.peltarion.tictactoe.exception.OtherPrayerMoveException;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.tictactoe.model.api.response.GameBoard;
import se.peltarion.tictactoe.model.game.GameStatus;
import se.peltarion.tictactoe.model.game.GameSymbol;
import se.peltarion.tictactoe.model.service.Move;
import se.peltarion.tictactoe.model.api.response.MoveResponse;
import se.peltarion.tictactoe.service.GameService;
import se.peltarion.tictactoe.testdata.DeletableRecord;
import se.peltarion.tictactoe.testdata.TestGameDataProvider;
import se.peltarion.tictactoe.testdata.TestGameRepositoryProvider;
import se.peltarion.tictactoe.testdata.TestMoveDataProvider;
import se.peltarion.tictactoe.testdata.TestMoveRepositoryProvider;
import se.peltarion.db.tables.records.GameRecord;
import se.peltarion.db.tables.records.MoveRecord;

@SpringBootTest
@AutoConfigureMockMvc
class GameControllerIntegrationTest {

  private static final String controllerPath = "/tictactoe";

  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private SimpMessagingTemplate testSimpMessagingTemplate;

  @Autowired
  private DSLContext testContext;

  @Autowired
  private GameService gameService;

  private TestGameRepositoryProvider testGameRepositoryProvider;
  private TestMoveRepositoryProvider testMoveRepositoryProvider;


  @BeforeEach
  public void setUp() {
    this.testGameRepositoryProvider = new TestGameRepositoryProvider(this.testContext);
    this.testMoveRepositoryProvider = new TestMoveRepositoryProvider(this.testContext);
    this.mockMvc = MockMvcBuilders.standaloneSetup(new GameController(this.gameService, this.testSimpMessagingTemplate)).build();
  }

  @Test
  void testShouldReturnGameBoardOnGettingGameBoard() throws Exception {
    final String endpoint = controllerPath + "/get-game-board";

    try (
        final DeletableRecord<GameRecord> testGameRecord =
            new DeletableRecord<>(this.testGameRepositoryProvider.insertGame(TestGameDataProvider.getSampleGame()));
        final DeletableRecord<MoveRecord> testMoveRecord =
            new DeletableRecord<>(this.testMoveRepositoryProvider.insertMove(TestMoveDataProvider.getMoveByUserUuidAndPlayerUuid(testGameRecord.get().getUuid(), testGameRecord.get().getPlayerOneUuid())))
    ) {
      final Game testGame = testGameRecord.get().into(Game.class);
      final Move testMove = testMoveRecord.get().into(Move.class);

      final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(endpoint)
          .header("userUuid", UUID.randomUUID())
          .queryParam("gameUuid", testGame.getUuid().toString())
      ).andReturn();

      final GameBoard responseGameBoard = this.objectMapper
          .readValue(mvcResult
                  .getResponse()
                  .getContentAsString(),
              new TypeReference<GameBoard>() {
              });

      assertThat(responseGameBoard).isNotNull();
      assertThat(responseGameBoard.getGame().getUuid()).isEqualTo(testGame.getUuid());
      assertThat(responseGameBoard.getGame().getPlayerOneUuid()).isEqualTo(testGame.getPlayerOneUuid());
      assertThat(responseGameBoard.getGame().getStatus()).isEqualTo(testGame.getStatus());
      assertThat(responseGameBoard.getGame().getCreated()).isEqualTo(testGame.getCreated());

      assertThat(responseGameBoard.getMoves().get(0).getUuid()).isEqualTo(testMove.getUuid());
      assertThat(responseGameBoard.getMoves().get(0).getPlayerUuid()).isEqualTo(testMove.getPlayerUuid());
      assertThat(responseGameBoard.getMoves().get(0).getRow()).isEqualTo(testMove.getRow());
      assertThat(responseGameBoard.getMoves().get(0).getCol()).isEqualTo(testMove.getCol());
      assertThat(responseGameBoard.getMoves().get(0).getCreated()).isEqualTo(testMove.getCreated());

    }
  }

  @Test
  void testShouldReturnAvailableGameListOnGettingAvailableGames() throws Exception {
    final String endpoint = controllerPath + "/get-available-games";

    try (
        final DeletableRecord<GameRecord> testGameRecord =
            new DeletableRecord<>(this.testGameRepositoryProvider.insertGame(TestGameDataProvider.getSampleGame()))
    ) {
      final Game testGame = testGameRecord.get().into(Game.class);

      final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(endpoint)
          .header("userUuid", UUID.randomUUID())
      ).andReturn();

      final List<Game> responseGames = this.objectMapper
          .readValue(mvcResult
                  .getResponse()
                  .getContentAsString(),
              new TypeReference<List<Game>>() {
              });

      assertThat(responseGames).isNotNull();
      assertThat(responseGames.size()).isPositive();

      final Game responseGame =
          responseGames.stream()
              .filter(game -> game.getUuid().equals(testGame.getUuid()))
              .findFirst()
              .orElseThrow(null);

      assertThat(responseGame).isNotNull();
      assertThat(responseGame.getUuid()).isEqualTo(testGame.getUuid());
      assertThat(responseGame.getPlayerOneUuid()).isEqualTo(testGame.getPlayerOneUuid());
      assertThat(responseGame.getStatus()).isEqualTo(testGame.getStatus());
      assertThat(responseGame.getCreated()).isEqualTo(testGame.getCreated());

    }
  }

  @Test
  void testShouldReturnValidGameOnCreatingGame() throws Exception {
    final String endpoint = controllerPath + "/create-game";
    final UUID playerOneUuid = UUID.randomUUID();
    try {
      final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
          .header("userUuid", playerOneUuid)
      ).andReturn();

      final Game responseGame = this.objectMapper
          .readValue(mvcResult
                  .getResponse()
                  .getContentAsString(),
              new TypeReference<Game>() {
              });

      assertThat(responseGame).isNotNull();
      assertThat(responseGame.getStatus()).isEqualTo(GameStatus.NOT_STARTED.getDescription());
      assertThat(responseGame.getWinner()).isNull();
      assertThat(responseGame.getPlayerOneUuid()).isEqualTo(playerOneUuid);

    } finally {
      this.testGameRepositoryProvider.deleteGameByPlayerUuid(playerOneUuid);
    }
  }

  @Test
  void testShouldReturnValidMoveOnTalkingMove() throws Exception {
    final String endpoint = controllerPath + "/move";

    try (
        final DeletableRecord<GameRecord> testGameRecord =
            new DeletableRecord<>(this.testGameRepositoryProvider.insertGame(TestGameDataProvider.getSampleGameWithPlayStatus()))
    ) {
      final Game testGame = testGameRecord.get().into(Game.class);
      final int row = 0;
      final int col = 0;

      final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
          .header("userUuid", testGame.getPlayerOneUuid())
          .queryParam("gameUuid", testGame.getUuid().toString())
          .queryParam("row", String.valueOf(row))
          .queryParam("col", String.valueOf(col))
      ).andReturn();

      final MoveResponse responseMove = this.objectMapper
          .readValue(mvcResult
                  .getResponse()
                  .getContentAsString(),
              new TypeReference<MoveResponse>() {
              });

      assertThat(responseMove).isNotNull();
      assertThat(responseMove.getMove().getPlayerUuid()).isEqualTo(testGame.getPlayerOneUuid());
      assertThat(responseMove.getMove().getRow()).isEqualTo(row);
      assertThat(responseMove.getMove().getCol()).isEqualTo(col);

      this.testMoveRepositoryProvider.deleteMoveByPlayerUuid(testGame.getPlayerOneUuid());
    }
  }

  @Test
  void testShouldReturnValidGameOnJoiningGame() throws Exception {
    final String endpoint = controllerPath + "/join-game";

    try (
        final DeletableRecord<GameRecord> testGameRecord =
            new DeletableRecord<>(this.testGameRepositoryProvider.insertGame(TestGameDataProvider.getSampleGame()))
    ) {
      final Game testGame = testGameRecord.get().into(Game.class);
      final UUID playerTwoUuid = UUID.randomUUID();
      final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.put(endpoint)
          .header("userUuid", playerTwoUuid)
          .queryParam("gameUuid", testGame.getUuid().toString())
      ).andReturn();

      final Game responseGame = this.objectMapper
          .readValue(mvcResult
                  .getResponse()
                  .getContentAsString(),
              new TypeReference<Game>() {
              });

      assertThat(responseGame.getUuid()).isEqualTo(testGame.getUuid());
      assertThat(responseGame.getPlayerOneUuid()).isEqualTo(testGame.getPlayerOneUuid());
      assertThat(responseGame.getStatus()).isEqualTo(GameStatus.PLAY.getDescription());
      assertThat(responseGame.getPlayerTwoUuid()).isEqualTo(playerTwoUuid);
      assertThat(responseGame.getCreated()).isEqualTo(testGame.getCreated());
      assertThat(responseGame.getPlayerOneSymbol()).isEqualTo(GameSymbol.PLAYER_ONE.getSymbol());
      assertThat(responseGame.getPlayerTwoSymbol()).isEqualTo(GameSymbol.PLAYER_TWO.getSymbol());
    }
  }

  @Test
  void testShouldReturnGameNotAvailableExceptionOnGettingGameBoard() throws Exception {
    final String endpoint = controllerPath + "/get-game-board";

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(endpoint)
        .header("userUuid", UUID.randomUUID())
        .queryParam("gameUuid", UUID.randomUUID().toString())
    ).andReturn();

    assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    assertThat(Objects.requireNonNull(mvcResult.getResolvedException())
        .getMessage()).isEqualTo(new GameNotAvailableException().getMessage());

  }

  @Test
  void testShouldReturnGameNotAvailableExceptionOnTakingMove() throws Exception {
    final String endpoint = controllerPath + "/move";

    final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
        .header("userUuid", UUID.randomUUID().toString())
        .queryParam("gameUuid", UUID.randomUUID().toString())
        .queryParam("row", String.valueOf(0))
        .queryParam("col", String.valueOf(0))
    ).andReturn();

    assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    assertThat(Objects.requireNonNull(mvcResult.getResolvedException())
        .getMessage()).isEqualTo(new GameNotAvailableException().getMessage());

  }

  @Test
  void testShouldReturnOtherPrayerMoveExceptionOnTakingMove() throws Exception {
    final String endpoint = controllerPath + "/move";

    try (
        final DeletableRecord<GameRecord> testGameRecord =
            new DeletableRecord<>(this.testGameRepositoryProvider.insertGame(TestGameDataProvider.getSampleGameWithPlayStatus()))
    ) {
      final Game testGame = testGameRecord.get().into(Game.class);

      final MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(endpoint)
          .header("userUuid", UUID.randomUUID())
          .queryParam("gameUuid", testGame.getUuid().toString())
          .queryParam("row", String.valueOf(0))
          .queryParam("col", String.valueOf(0))
      ).andReturn();

      assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
      assertThat(Objects.requireNonNull(mvcResult.getResolvedException())
          .getMessage()).isEqualTo(new OtherPrayerMoveException().getMessage());

    }
  }

}
