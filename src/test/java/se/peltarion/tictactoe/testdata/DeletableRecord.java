package se.peltarion.tictactoe.testdata;

import org.jooq.impl.UpdatableRecordImpl;

public class DeletableRecord <R extends UpdatableRecordImpl<R>> implements AutoCloseable {
  private final R closableRecord;

  public DeletableRecord(R closableRecord) {
    this.closableRecord = closableRecord;
  }

  public R get() {
    return this.closableRecord;
  }

  @Override
  public void close() {
    this.closableRecord.delete();
  }
}
