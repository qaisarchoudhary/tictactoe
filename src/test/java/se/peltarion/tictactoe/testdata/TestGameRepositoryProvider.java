package se.peltarion.tictactoe.testdata;

import static se.peltarion.db.tables.Game.GAME;

import java.util.UUID;
import org.jooq.DSLContext;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.db.tables.records.GameRecord;

public class TestGameRepositoryProvider {

  private final DSLContext superUserContext;

  public TestGameRepositoryProvider(DSLContext superUserContext) {
    this.superUserContext = superUserContext;
  }

  public void deleteGameByPlayerUuid(UUID playerOneUuid) {
    this.superUserContext.deleteFrom(GAME)
        .where(GAME.PLAYER_ONE_UUID.eq(playerOneUuid))
        .execute();
  }

  public GameRecord insertGame(Game testGame) {
    GameRecord gameRecord = this.superUserContext.newRecord(GAME, testGame);
    gameRecord.insert();
    return gameRecord;
  }

}
