package se.peltarion.tictactoe.testdata;

import java.util.UUID;
import se.peltarion.tictactoe.model.game.GameStatus;
import se.peltarion.tictactoe.model.service.Move;
import se.peltarion.tictactoe.model.api.response.MoveResponse;

public class TestMoveDataProvider {

  public static MoveResponse getMoveResponseByUserUuidAndPlayerUuid(UUID gameUuid, UUID playerUuid) {

    Move move = Move.Builder
        .uuid(UUID.randomUUID())
        .gameUuid(gameUuid)
        .row(0)
        .col(0)
        .playerUuid(playerUuid)
        .build();

    return MoveResponse.Builder
        .move(move)
        .winner(null)
        .status(GameStatus.PLAY.getDescription())
        .build();

  }

  public static Move getMoveByUserUuidAndPlayerUuid(UUID gameUuid, UUID playerUuid) {

    return Move.Builder
        .uuid(UUID.randomUUID())
        .gameUuid(gameUuid)
        .row(0)
        .col(0)
        .playerUuid(playerUuid)
        .build();
  }

}
