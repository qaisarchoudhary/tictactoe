package se.peltarion.tictactoe.testdata;

import static se.peltarion.db.tables.Move.MOVE;

import java.util.UUID;
import org.jooq.DSLContext;
import se.peltarion.tictactoe.model.service.Move;
import se.peltarion.db.tables.records.MoveRecord;

public class TestMoveRepositoryProvider {

  private final DSLContext superUserContext;

  public TestMoveRepositoryProvider(final DSLContext superUserContext) {
    this.superUserContext = superUserContext;
  }

  public MoveRecord insertMove(Move move) {
    MoveRecord moveRecord = this.superUserContext.newRecord(MOVE, move);
    moveRecord.insert();
    return moveRecord;
  }
  public void deleteMoveByPlayerUuid(final UUID playerOneUuid) {
    this.superUserContext.deleteFrom(MOVE)
        .where(MOVE.PLAYER_UUID.eq(playerOneUuid))
        .execute();
  }
}
