package se.peltarion.tictactoe.testdata;

import java.time.OffsetDateTime;
import java.util.UUID;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.tictactoe.model.game.GameStatus;
import se.peltarion.tictactoe.model.game.GameSymbol;

public class TestGameDataProvider {

  public static Game getSampleGame() {
    return Game.Builder
        .uuid(UUID.randomUUID())
        .playerOneUuid(UUID.randomUUID())
        .playerOneSymbol(GameSymbol.PLAYER_ONE.getSymbol())
        .status(GameStatus.NOT_STARTED.getDescription())
        .created(OffsetDateTime.now())
        .build();
  }

  public static Game getSampleGameWithPlayStatus() {
    UUID currentUser = UUID.randomUUID();
        return Game.Builder
            .uuid(UUID.randomUUID())
            .playerOneUuid(currentUser)
            .nextMovePlayerUuid(currentUser)
            .playerOneSymbol(GameSymbol.PLAYER_ONE.getSymbol())
            .playerTwoSymbol(GameSymbol.PLAYER_TWO.getSymbol())
            .status(GameStatus.PLAY.getDescription())
            .created(OffsetDateTime.now())
            .build();
  }

}
