let stompClient = null;
let baseEndpoint = '/tictactoe';
let symbols = [];

let GameStatus = {
  NOT_STARTED : {description: "Not started yet"},
  PLAY : {description: "Play"},
  FINISHED : { description: "Finished"},
  FINISHED_WITH_TIE: { description : "Finished with tie"}
};

function init() {
  let socket = new SockJS(baseEndpoint);
  stompClient = Stomp.over(socket);
  stompClient.connect({}, function (frame) {
    if(!$("#gameUuid").val()) {
      alert("Please provide gameUuid");
      return;
    }
    getGameBoard();
    stompClient.subscribe('/topic/move/'+$("#gameUuid").val(), receiveMove);
  });
}

function receiveMove (response) {
  let moveResponse = JSON.parse(response.body);
  updateGameBoard(moveResponse.lastMove.playerUuid, moveResponse.lastMove.row , moveResponse.lastMove.col);
  checkGameStatus(moveResponse.gameStatus , moveResponse.winner);

}
function checkGameStatus(gameStatus, winner ){
  if (gameStatus===GameStatus.FINISHED.description)
    $("#status").html("Player <b> "+winner+" </b> with symbol <b>"+symbols[winner]+"</b> wins");
  else if (gameStatus===GameStatus.FINISHED_WITH_TIE.description)
    $("#status").html("<b>Game finished with tie</b>");
}

function getGameBoard() {
  const gameUuid = $("#gameUuid").val();
  const userUuid = $("#userUuid").val();
  const options = {
    params: {
      gameUuid: gameUuid
    },
    headers: {
      "userUuid" : userUuid
    }
  };

  axios.get(baseEndpoint+"/get-game-board", options)
  .then((response) => {
    symbols[response.data.game.playerOneUuid] = response.data.game.playerOneSymbol;
    symbols[response.data.game.playerTwoUuid] = response.data.game.playerTwoSymbol;
    checkTurnPlayer(response.data.game.nextMovePlayerUuid);
    $("#symbol").append("Your Symbol is <b> "+ symbols[userUuid]+ "</b>");
    response.data.moves.forEach(move =>{
      updateGameBoard(move.playerUuid, move.row, move.col);
    })
  })
  .catch((exception) => {
    console.log("Exception in getGameBoard(): " +JSON.stringify(exception));
  });
}
function checkTurnPlayer(nextMovePlayerUuid){

  if ($("#userUuid").val()===nextMovePlayerUuid)
    $("#status").html("It's <b> YOUR </b> turn now");
  else
    $("#status").html("It's <b> OTHER</b> player's turn now ");
}

function move(cell) {
  const row = parseInt(cell.parent().index());
  const col = parseInt(cell.index());
  const gameUuid = $("#gameUuid").val();
  const userUuid = $("#userUuid").val();
  const options = {
    params: {
      gameUuid: gameUuid,
      row: row,
      col: col
    },
    headers: {
      "userUuid" : userUuid
    }
  };
  axios.post(baseEndpoint+"/move", {}, options)
  .then((response) => {
  })
  .catch((exception) => {
    console.log("Exception: " +JSON.stringify(exception));
  });
}

function updateGameBoard(playerUuid, row, col) {
  let symbol = symbols[playerUuid];
  $("#gameBoard").find('tr').eq(row).find("td").eq(col).html(symbol);

  const userUuid = $("#userUuid").val();
  console.log("userUuid: "+userUuid);

  checkTurnPlayer(userUuid===playerUuid ? null : userUuid);
}

$(function () {
  let height = $(window).height();
  $("#conversation").css("height",2*height/3);

  $("form").on('submit', function (e) {
    e.preventDefault();
  });
  $( "#start").click(function() { init(); });
  $( "#gameBoard td").click(function() {move($(this));});
});