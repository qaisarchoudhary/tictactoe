--
-- Name: game; Type: TABLE; Schema: tictactoe; Owner: devuser
--
CREATE TABLE tictactoe.game (
    uuid uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    player_one_uuid uuid,
    player_one_symbol character varying(200),
    player_two_uuid uuid,
    player_two_symbol character varying(200),
    next_move_player_uuid uuid,
    status character varying(200) NOT NULL,
    winner uuid,
    created timestamp WITH TIME ZONE
);

--
-- Name: cell; Type: TABLE; Schema: tictactoe; Owner: devuser
--
CREATE TABLE tictactoe.move (
    uuid uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    row integer,
    col integer,
    game_uuid uuid NOT NULL,
    player_uuid uuid NOT NULL,
    created timestamp WITH TIME ZONE,

    CONSTRAINT game_uuid_constraint
        FOREIGN KEY(game_uuid)
            REFERENCES tictactoe.game(uuid)

);