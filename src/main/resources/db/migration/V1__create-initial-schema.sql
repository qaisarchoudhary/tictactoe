
CREATE SCHEMA IF NOT EXISTS tictactoe;
create user tictactoeuser with password 'tictactoepwd';
create role tictactoerole;
grant tictactoerole to tictactoeuser;

GRANT USAGE ON SCHEMA tictactoe TO tictactoerole;


GRANT USAGE ON SCHEMA tictactoe TO tictactoeuser;
GRANT ALL PRIVILEGES ON SCHEMA tictactoe TO tictactoeuser;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA tictactoe TO tictactoeuser;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA tictactoe TO tictactoeuser;

-- Add extension to enable uuid if not enabled
CREATE EXTENSION "uuid-ossp";
