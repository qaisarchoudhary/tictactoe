package se.peltarion.tictactoe.model.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import java.time.OffsetDateTime;
import java.util.UUID;

public class Move {

  @JsonProperty("uuid")
  private final UUID uuid;
  @JsonProperty("row")
  private final int row;
  @JsonProperty("col")
  private final int col;
  @JsonIgnore
  private final UUID gameUuid;
  @JsonProperty("playerUuid")
  private final UUID playerUuid;
  @JsonProperty("created")
  private final OffsetDateTime created;

  public Move(
      UUID uuid,
      int row,
      int col,
      UUID gameUuid,
      UUID playerUuid,
      OffsetDateTime created)
  {
    this.uuid = uuid;
    this.row=row;
    this.col=col;
    this.gameUuid=gameUuid;
    this.playerUuid=playerUuid;
    this.created = created;
  }

  public UUID getUuid() {
    return this.uuid;
  }

  public int getRow() {
    return this.row;
  }

  public int getCol() {
    return this.col;
  }

  public UUID getGameUuid() {
    return this.gameUuid;
  }

  public UUID getPlayerUuid() {
    return this.playerUuid;
  }

  public OffsetDateTime getCreated() {
    return this.created;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("uuid", this.uuid)
        .add("row", this.row)
        .add("col", this.col)
        .add("gameUuid", this.gameUuid)
        .add("playerUuid", this.playerUuid)
        .add("created", this.created)
        .toString();
  }

  public static class Builder {

    private final UUID uuid;
    private int row;
    private int col;
    private  UUID gameUuid;
    private  UUID playerUuid;
    private  OffsetDateTime created;

    public Builder(UUID uuid) {
      this.uuid = uuid;
    }

    public static Builder uuid(UUID uuid) {
      return new Builder(uuid);
    }

    public Builder row(int row) {
      this.row = row;
      return this;
    }

    public Builder col(int col) {
      this.col = col;
      return this;
    }

    public Builder gameUuid(UUID gameUuid) {
      this.gameUuid = gameUuid;
      return this;
    }

    public Builder playerUuid(UUID playerUuid) {
      this.playerUuid = playerUuid;
      return this;
    }


    public Builder created(OffsetDateTime created) {
      this.created = created;
      return this;
    }

    public Move build() {
      return new Move(
          this.uuid,
          this.row,
          this.col,
          this.gameUuid,
          this.playerUuid,
          this.created);
    }
  }
}
