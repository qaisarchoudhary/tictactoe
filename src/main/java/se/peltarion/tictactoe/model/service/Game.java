package se.peltarion.tictactoe.model.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import java.time.OffsetDateTime;
import java.util.UUID;

public class Game {
  @JsonProperty("uuid")
  private final UUID uuid;
  @JsonProperty("playerOneUuid")
  private final UUID playerOneUuid;
  @JsonProperty("playerOneSymbol")
  private final String playerOneSymbol;
  @JsonProperty("playerTwoUuid")
  private final UUID playerTwoUuid;
  @JsonProperty("playerTwoSymbol")
  private final String playerTwoSymbol;
  @JsonProperty("nextMovePlayerUuid")
  private final UUID nextMovePlayerUuid;
  @JsonProperty("status")
  private final String status;
  @JsonProperty("winner")
  private final UUID winner;
  @JsonProperty("created")
  private final OffsetDateTime created;

  public Game(
      UUID uuid,
      UUID playerOneUuid,
      String playerOneSymbol,
      UUID playerTwoUuid,
      String playerTwoSymbol,
      UUID nextMovePlayerUuid,
      String status,
      UUID winner,
      OffsetDateTime created)
  {
    this.uuid = uuid;
    this.playerOneUuid = playerOneUuid;
    this.playerOneSymbol=playerOneSymbol;
    this.playerTwoUuid = playerTwoUuid;
    this.playerTwoSymbol=playerTwoSymbol;
    this.nextMovePlayerUuid = nextMovePlayerUuid;
    this.status = status;
    this.winner=winner;
    this.created = created;
  }

  public UUID getUuid() {
    return this.uuid;
  }

  public UUID getPlayerOneUuid() {
    return this.playerOneUuid;
  }

  public String getPlayerOneSymbol() {
    return this.playerOneSymbol;
  }

  public UUID getPlayerTwoUuid() {
    return this.playerTwoUuid;
  }

  public String getPlayerTwoSymbol() {
    return this.playerTwoSymbol;
  }

  public UUID getNextMovePlayerUuid() {
    return this.nextMovePlayerUuid;
  }

  public String getStatus() {
    return this.status;
  }

  public UUID getWinner() {
    return this.winner;
  }

  public OffsetDateTime getCreated() {
    return this.created;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("uuid", this.uuid)
        .add("playerOneUuid", this.playerOneUuid)
        .add("playerOneSymbol", this.playerOneSymbol)
        .add("playerTwoUuid", this.playerTwoUuid)
        .add("playerTwoSymbol", this.playerTwoSymbol)
        .add("nextMovePlayerUuid", this.nextMovePlayerUuid)
        .add("status", this.status)
        .add("winner", this.winner)
        .add("created", this.created)
        .toString();
  }

  public static class Builder {

    private UUID uuid;
    private  UUID playerOneUuid;
    private  String playerOneSymbol;
    private  UUID playerTwoUuid;
    private  String playerTwoSymbol;
    private  UUID nextMovePlayerUuid;
    private  String status;
    private UUID winner;
    private  OffsetDateTime created;

    public Builder(UUID uuid) {
      this.uuid = uuid;
    }

    public static Builder uuid(UUID uuid) {
      return new Builder(uuid);
    }

    public Builder playerOneUuid(UUID playerOneUuid) {
      this.playerOneUuid = playerOneUuid;
      return this;
    }

    public Builder playerOneSymbol(String playerOneSymbol) {
      this.playerOneSymbol = playerOneSymbol;
      return this;
    }

    public Builder playerTwoUuid(UUID playerTwoUuid) {
      this.playerTwoUuid = playerTwoUuid;
      return this;
    }

    public Builder playerTwoSymbol(String playerTwoSymbol) {
      this.playerTwoSymbol = playerTwoSymbol;
      return this;
    }

    public Builder nextMovePlayerUuid(UUID nextMovePlayerUuid) {
      this.nextMovePlayerUuid = nextMovePlayerUuid;
      return this;
    }

    public Builder status(String status) {
      this.status = status;
      return this;
    }

    public Builder winner(UUID winner) {
      this.winner = winner;
      return this;
    }

    public Builder created(OffsetDateTime created) {
      this.created = created;
      return this;
    }

    public Game build() {
      return new Game(
          this.uuid,
          this.playerOneUuid,
          this.playerOneSymbol,
          this.playerTwoUuid,
          this.playerTwoSymbol,
          this.nextMovePlayerUuid,
          this.status,
          this.winner,
          this.created);
    }
  }
}
