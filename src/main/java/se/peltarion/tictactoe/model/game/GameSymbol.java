package se.peltarion.tictactoe.model.game;

public enum GameSymbol {

  PLAYER_ONE("X"),
  PLAYER_TWO("O");

  private final String symbol;

  GameSymbol(String symbol) {
    this.symbol = symbol;
  }

  public String getSymbol() {
    return this.symbol;
  }
}
