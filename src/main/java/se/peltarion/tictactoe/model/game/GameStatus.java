package se.peltarion.tictactoe.model.game;

import java.util.stream.Stream;

public enum GameStatus {

  NOT_STARTED("Not started yet"),
  PLAY("Play"),
  FINISHED("Finished"),
  FINISHED_WITH_TIE("Finished with tie");

  private final String description;

  GameStatus(String description) {
    this.description = description;
  }

  public String getDescription() {
    return this.description;
  }

  public static GameStatus getGameStatusByDescription(String description){
    return Stream.of(values())
        .filter(gameStatus->gameStatus.description.equals(description))
        .findFirst().orElse(null);
  }
}
