package se.peltarion.tictactoe.model.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import java.util.List;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.tictactoe.model.service.Move;

public class GameBoard {

  @JsonProperty("game")
  private final Game game;
  @JsonProperty("moves")
  private final List<Move> moves;


  public GameBoard(
      Game game,
      List<Move> moves
  )
  {
    this.game = game;
    this.moves = moves;

  }

  public Game getGame() {
    return this.game;
  }

  public List<Move> getMoves() {
    return this.moves;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("game", this.game)
        .add("moves", this.moves)
        .toString();
  }

  public static class Builder {

    private final Game game;
    private List<Move> moves;

    public Builder(Game game) {
      this.game = game;
    }

    public static Builder game(Game game) {
      return new Builder(game);
    }

    public Builder moves(List<Move> moves) {
      this.moves = moves;
      return this;
    }

    public GameBoard build() {
      return new GameBoard(
          this.game,
          this.moves
      );
    }

  }

}
