package se.peltarion.tictactoe.model.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import java.util.UUID;
import se.peltarion.tictactoe.model.service.Move;

public class MoveResponse {
  @JsonProperty("gameStatus")
  private final String status;
  @JsonProperty("winner")
  private final UUID winner;
  @JsonProperty("lastMove")
  private final Move move;

  public MoveResponse(
      String status,
      UUID winner,
      Move move
  )
  {
    this.status = status;
    this.winner=winner;
    this.move = move;
  }

  public String getStatus() {
    return this.status;
  }

  public UUID getWinner() {
    return this.winner;
  }

  public Move getMove() {
    return this.move;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("status", this.status)
        .add("winner", this.winner)
        .add("lastMove", this.move)
        .toString();
  }

  public static class Builder {

    private final Move move;
    private String status;
    private UUID winner;

    public Builder(Move move) {
      this.move = move;
    }

    public static Builder move(Move move) {
      return new Builder(move);
    }

    public Builder status(String status) {
      this.status = status;
      return this;
    }

    public Builder winner(UUID winner) {
      this.winner = winner;
      return this;
    }

    public MoveResponse build() {
      return new MoveResponse(
          this.status,
          this.winner,
          this.move
      );
    }

  }
}
