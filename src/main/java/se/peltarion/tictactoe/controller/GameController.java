package se.peltarion.tictactoe.controller;

import java.util.List;
import java.util.UUID;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.tictactoe.model.api.response.GameBoard;
import se.peltarion.tictactoe.model.api.response.MoveResponse;
import se.peltarion.tictactoe.service.GameService;

@RestController
@RequestMapping("/tictactoe")
@Validated
public class GameController {

  private final GameService gameService;
  private final SimpMessagingTemplate simpMessagingTemplate;

  @Autowired
  public GameController(GameService gameService, SimpMessagingTemplate simpMessagingTemplate) {
    this.gameService = gameService;
    this.simpMessagingTemplate = simpMessagingTemplate;
  }

  @PostMapping("/create-game")
  public Game createGame(
      @RequestHeader("userUuid") UUID userUuid
  )
  {
    return this.gameService.createGame(userUuid);
  }

  @GetMapping("/get-available-games")
  public List<Game> getAvailableGames() {
    return this.gameService.getAvailableGames();
  }

  @PutMapping("/join-game")
  public Game joinGame(
      @RequestHeader("userUuid") UUID userUuid,
      @RequestParam("gameUuid") UUID gameUuid
  )
  {
    return this.gameService.joinGame(userUuid, gameUuid);
  }

  @PostMapping("/move")
  public MoveResponse move(
      @RequestHeader("userUuid") UUID userUuid,
      @RequestParam("gameUuid") UUID gameUuid,
      @RequestParam("row") @Min(0)@Max(2) int row,
      @RequestParam("col") @Min(0) @Max(2) int col
  )
  {
    MoveResponse moveResponse = this.gameService.move(userUuid, gameUuid, row, col);
    this.simpMessagingTemplate.convertAndSend("/topic/move/"+gameUuid, moveResponse);
    return moveResponse;
  }

  @GetMapping("/get-game-board")
  public GameBoard getGameBoard(
      @RequestHeader("userUuid") UUID userUuid,
      @RequestParam("gameUuid") UUID gameUuid
  )
  {
    return this.gameService.getGameBoard(userUuid, gameUuid);
  }

}
