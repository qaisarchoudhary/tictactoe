package se.peltarion.tictactoe.game;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Component;
import se.peltarion.tictactoe.model.game.GameStatus;
import se.peltarion.tictactoe.model.service.Move;

@Component
public class GameEngine {

  private static final int rows = 3;
  private static final int cols = 3;

  public GameStatus checkGameStatus(final List<Move> moves) {
    final UUID[][] movesArray = this.convertListToArray(moves);

    for (int i = 0; i < 3; i++) {
      if ((movesArray[i][0] != null) && (movesArray[i][0].equals(movesArray[i][1]) && (movesArray[i][0].equals(
          movesArray[i][2])))) {
        return GameStatus.FINISHED;
      }

      if ((movesArray[0][i] != null) && (movesArray[0][i].equals(movesArray[1][i]) && movesArray[0][i].equals(movesArray[2][i]))) {
        return GameStatus.FINISHED;
      }
    }

    if ((movesArray[0][0] != null) && (movesArray[0][0].equals(movesArray[1][1]) && (movesArray[0][0].equals(movesArray[2][2])))) {
      return GameStatus.FINISHED;
    }

    if ((movesArray[2][0] != null) && (movesArray[2][0].equals(movesArray[1][1]) && (movesArray[2][0].equals(movesArray[0][2])))) {
      return GameStatus.FINISHED;
    }

    if (moves.size() == (rows * cols)) { return GameStatus.FINISHED_WITH_TIE; }

    return GameStatus.PLAY;

  }

  private UUID[][] convertListToArray(final List<Move> moves) {
    final UUID[][] movesArray = new UUID[rows][cols];
    moves.forEach(move -> movesArray[move.getRow()][move.getCol()] = move.getPlayerUuid());
    return movesArray;
  }

  private void print(final UUID[][] movesArray) {
    Arrays.stream(movesArray).forEach(row -> {
      Arrays.stream(row).forEach(col -> System.out.print(col + " "));
      System.out.println();
    });
  }

}
