package se.peltarion.tictactoe.repository;

import static se.peltarion.db.tables.Move.MOVE;

import java.util.UUID;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.springframework.stereotype.Repository;
import se.peltarion.tictactoe.model.service.Move;
import se.peltarion.db.tables.records.MoveRecord;

@Repository
public class MoveRepository {

  public MoveRecord move(DSLContext context, Move newMove) {
    MoveRecord moveRecord = context.newRecord(MOVE, newMove);
    moveRecord.insert();
    return moveRecord;
  }

  public MoveRecord getMoveByCoordinates(DSLContext context, UUID gameUuid, int row, int col) {
    return context.selectFrom(MOVE)
        .where(MOVE.GAME_UUID.eq(gameUuid))
        .and(MOVE.ROW.eq(row))
        .and(MOVE.COL.eq(col))
        .fetchOne();
  }

  public Result<MoveRecord> getMovesByGameUuid(DSLContext context, UUID gameUuid) {
    return context.selectFrom(MOVE)
        .where(MOVE.GAME_UUID.eq(gameUuid))
        .fetch();
  }
}
