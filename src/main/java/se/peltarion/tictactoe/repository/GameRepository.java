package se.peltarion.tictactoe.repository;

import static se.peltarion.db.Tables.GAME;
import java.util.UUID;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.springframework.stereotype.Repository;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.tictactoe.model.game.GameStatus;
import se.peltarion.db.tables.records.GameRecord;

@Repository
public class GameRepository{

  public GameRecord createGame(DSLContext context, Game game){
    GameRecord gameRecord = context.newRecord(GAME, game);
    gameRecord.insert();
    return gameRecord;
  }

  public Result<GameRecord> getAvailableGames(DSLContext context){
    return context.selectFrom(GAME)
        .where(GAME.STATUS.eq(GameStatus.NOT_STARTED.getDescription()))
        .fetch();
  }

  public GameRecord getGameByUuid(DSLContext context, UUID gameUuid) {
    return context.selectFrom(GAME)
        .where(GAME.UUID.eq(gameUuid))
        .fetchOne();
  }

  public GameRecord getAvailableGameByUuid(DSLContext context, UUID gameUuid) {
    return context.selectFrom(GAME)
        .where(GAME.UUID.eq(gameUuid))
        .and(GAME.STATUS.eq(GameStatus.NOT_STARTED.getDescription()))
        .fetchOne();
  }

  public GameRecord updateGame(DSLContext context, Game game) {
    GameRecord gameRecord = context.newRecord(GAME, game);
    gameRecord.update();
    return gameRecord;
  }
}
