package se.peltarion.tictactoe.configuration;

import java.util.Map;
import javax.validation.constraints.NotNull;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.RecordMapperProvider;
import org.jooq.RecordType;
import org.jooq.impl.DefaultRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ResolvableType;

@Configuration
public class JooqRecordMapperProvider implements RecordMapperProvider {

  private final Map<String, RecordMapper> recordMappers;

  @Autowired
  public JooqRecordMapperProvider(Map<String, RecordMapper> recordMappers) {
    this.recordMappers = recordMappers;
  }

  @Override
  @Cacheable("recordMapper")
  public @NotNull <R extends Record, E> RecordMapper provide(RecordType<R> recordType, Class<? extends E> type) {
    return this.recordMappers
        .values().stream()
        .filter(recordMapper -> {
          final ResolvableType resolvableMapperType = ResolvableType.forClass(recordMapper.getClass()).as(RecordMapper.class);
          final ResolvableType resolvableRecordType = ResolvableType.forClass(recordType.getClass());
          return resolvableRecordType.resolveGeneric(0).isAssignableFrom(resolvableMapperType.resolveGeneric(0))
              && resolvableMapperType.resolveGeneric(1).isAssignableFrom(type);
        })
        .findFirst()
        .orElse(new DefaultRecordMapper<>(recordType, type));
  }

}