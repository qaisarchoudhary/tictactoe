//package se.pelarion.tictactoe.configuration;
//
//import java.util.Map;
//import org.jooq.Record;
//import org.jooq.RecordMapper;
//import org.jooq.RecordType;
//import org.jooq.RecordUnmapper;
//import org.jooq.RecordUnmapperProvider;
//import org.jooq.impl.DefaultRecordMapper;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.ResolvableType;
//import springfox.documentation.annotations.Cacheable;
//
//@Configuration
//public class JooqRecordUnmapperProvider implements RecordUnmapperProvider {
//
//
//  private final Map<String, RecordUnmapper> recordMappers;
//
//  public JooqRecordUnmapperProvider(Map<String, RecordUnmapper> recordMappers) {
//    this.recordMappers = recordMappers;
//  }
//
//  @Override
//  @Cacheable("recordUnmapper")
//  public <E, R extends Record> RecordUnmapper provide(Class<? extends E> type, RecordType<R> recordType)
//  {
//
//    return this.recordMappers
//        .values().stream()
//        .filter(recordUnmapper -> {
//          System.err.println("recordUnmapper: " + recordUnmapper);
//          final ResolvableType resolvableUnmapperType = ResolvableType.forClass(recordType.getClass());
//          System.err.println("resolvableUnmapperType: " + resolvableUnmapperType);
//          final ResolvableType resolvableRecordType = ResolvableType.forClass(recordUnmapper.getClass()).as(RecordUnmapper.class);
//          System.err.println("resolvableUnRecordType: " + resolvableRecordType);
//
//          System.err.println("resolvableRecordType.resolveGeneric(0) = " +resolvableRecordType.resolveGeneric(0));
//          System.err.println("resolvableUnmapperType.resolveGeneric(0) = " +resolvableUnmapperType.resolveGeneric(0));
//
//          System.err.println("resolvableMapperType.resolveGeneric(1) = " +resolvableRecordType.resolveGeneric(0));
//          System.err.println("type = " +type);
//
//          return resolvableRecordType.resolveGeneric(0).isAssignableFrom(resolvableUnmapperType.resolveGeneric(0))
//              && resolvableUnmapperType.resolveGeneric(1).isAssignableFrom(type);
//        })
//        .findFirst()
//        .get();
//        //.orElse(new DefaultRecordMapper<>(recordType, type));
//
//    //return new GameUnMapper();
//  }
//
//}