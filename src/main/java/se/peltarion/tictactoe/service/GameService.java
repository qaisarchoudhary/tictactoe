package se.peltarion.tictactoe.service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.peltarion.tictactoe.exception.GameNotAvailableException;
import se.peltarion.tictactoe.exception.MoveAlreadyMarkedException;
import se.peltarion.tictactoe.exception.OtherPrayerMoveException;
import se.peltarion.tictactoe.game.GameEngine;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.tictactoe.model.api.response.GameBoard;
import se.peltarion.tictactoe.model.game.GameStatus;
import se.peltarion.tictactoe.model.game.GameSymbol;
import se.peltarion.tictactoe.model.service.Move;
import se.peltarion.tictactoe.model.api.response.MoveResponse;
import se.peltarion.tictactoe.repository.GameRepository;
import se.peltarion.tictactoe.repository.MoveRepository;
import se.peltarion.db.tables.records.GameRecord;
import se.peltarion.db.tables.records.MoveRecord;

@Service
public class GameService {

  private final DSLContext context;
  private final GameRepository gameRepository;
  private final MoveRepository moveRepository;
  private final GameEngine gameEngine;

  @Autowired
  public GameService(
      DSLContext context,
      GameRepository gameRepository,
      MoveRepository moveRepository,
      GameEngine gameEngine)
  {
    this.context = context;
    this.gameRepository = gameRepository;
    this.moveRepository = moveRepository;
    this.gameEngine = gameEngine;
  }

  public Game createGame(final UUID userUuid) {

    final Game newGame = Game.Builder
        .uuid(UUID.randomUUID())
        .playerOneUuid(userUuid)
        .playerOneSymbol(GameSymbol.PLAYER_ONE.getSymbol())
        .status(GameStatus.NOT_STARTED.getDescription())
        .created(OffsetDateTime.now())
        .build();

    return this.gameRepository
        .createGame(this.context, newGame)
        .into(Game.class);
  }

  public List<Game> getAvailableGames() {
    return this.gameRepository
        .getAvailableGames(this.context)
        .into(Game.class);
  }

  public Game joinGame(UUID userUuid, UUID gameUuid) {

    final GameRecord updatedGameRecord = this.context.transactionResult(configuration -> {
      final DSLContext transactionContext = DSL.using(configuration);
      final GameRecord gameRecord = this.gameRepository.getAvailableGameByUuid(transactionContext, gameUuid);

      if (gameRecord == null) { throw new GameNotAvailableException(); }

      final UUID firstMovePlayer = this.decideFirstMovePlayer(gameRecord.getPlayerOneUuid(), userUuid);

      final Game updatedGame = Game.Builder
          .uuid(gameRecord.getUuid())
          .playerOneUuid(gameRecord.getPlayerOneUuid())
          .playerOneSymbol(gameRecord.getPlayerOneSymbol())
          .playerTwoUuid(userUuid)
          .playerTwoSymbol(GameSymbol.PLAYER_TWO.getSymbol())
          .nextMovePlayerUuid(firstMovePlayer)
          .status(GameStatus.PLAY.getDescription())
          .created(gameRecord.getCreated())
          .build();

      return this.gameRepository.updateGame(transactionContext, updatedGame);
    });

    return updatedGameRecord.into(Game.class);
  }

  public GameBoard getGameBoard(UUID userUuid, UUID gameUuid) {

    GameRecord gameRecord = this.gameRepository.getGameByUuid(this.context, gameUuid);
    if (gameRecord == null) { throw new GameNotAvailableException(); }
    Game game = gameRecord.into(Game.class);
    List<Move> moves = this.moveRepository.getMovesByGameUuid(this.context, gameUuid).into(Move.class);

    return GameBoard.Builder
        .game(game)
        .moves(moves)
        .build();
  }

  public MoveResponse move(UUID userUuid, UUID gameUuid, int row, int col) {
    return this.context.transactionResult(configuration -> {
      final DSLContext transactionContext = DSL.using(configuration);
      final GameRecord gameRecord = this.gameRepository.getGameByUuid(transactionContext, gameUuid);

      this.checkValidMoveCriteria(transactionContext, userUuid, row, col, gameRecord);

      final Move newMove = Move.Builder
          .uuid(UUID.randomUUID())
          .row(row)
          .col(col)
          .gameUuid(gameUuid)
          .playerUuid(userUuid)
          .created(OffsetDateTime.now())
          .build();

      final MoveRecord moveRecord = this.moveRepository.move(transactionContext, newMove);
      final Game updatedGame = this.checkGameStatusAndUpdateGameProperties(transactionContext, userUuid, gameRecord);

      return MoveResponse.Builder
          .move(moveRecord.into(Move.class))
          .status(updatedGame.getStatus())
          .winner(updatedGame.getWinner())
          .build();
    });
  }

  private UUID decideFirstMovePlayer(UUID playerOneUuid, UUID playerTwoUuid) {
    final boolean shouldPlayerOneMoveFirst = new Random(System.currentTimeMillis()).nextBoolean();
    return shouldPlayerOneMoveFirst ? playerOneUuid : playerTwoUuid;
  }

  private void checkValidMoveCriteria(
      DSLContext transactionContext,
      UUID userUuid,
      int row,
      int col,
      GameRecord gameRecord)
  {
    if (gameRecord == null || !gameRecord.getStatus().equals(GameStatus.PLAY.getDescription())) {
      throw new GameNotAvailableException();
    }
    if (!gameRecord.getNextMovePlayerUuid().equals(userUuid)) { throw new OtherPrayerMoveException(); }
    final MoveRecord existingMoveRecord =
        this.moveRepository.getMoveByCoordinates(transactionContext, gameRecord.getUuid() , row, col);
    if (existingMoveRecord != null) {throw new MoveAlreadyMarkedException();}
  }

  private Game checkGameStatusAndUpdateGameProperties(DSLContext context, UUID userUuid, GameRecord gameRecord) {

    UUID nextMovePlayerUuid = gameRecord.getPlayerOneUuid().equals(userUuid) ? gameRecord.getPlayerTwoUuid() : gameRecord.getPlayerOneUuid();
    List<Move> gameMoves = this.moveRepository.getMovesByGameUuid(context, gameRecord.getUuid()).into(Move.class);
    GameStatus gameStatus = this.gameEngine.checkGameStatus(gameMoves);

    final Game updatedGame = Game.Builder
        .uuid(gameRecord.getUuid())
        .playerOneUuid(gameRecord.getPlayerOneUuid())
        .playerOneSymbol(gameRecord.getPlayerOneSymbol())
        .playerTwoUuid(gameRecord.getPlayerTwoUuid())
        .playerTwoSymbol(gameRecord.getPlayerTwoSymbol())
        .nextMovePlayerUuid(gameStatus == GameStatus.FINISHED ? null : nextMovePlayerUuid)
        .status(gameStatus.getDescription())
        .winner(gameStatus==GameStatus.FINISHED ? userUuid : null)
        .created(gameRecord.getCreated())
        .build();

    return this.gameRepository.updateGame(context, updatedGame).into(Game.class);
  }

}