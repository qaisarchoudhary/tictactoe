package se.peltarion.tictactoe.service.recordmapper;

import org.jooq.RecordMapper;
import org.springframework.stereotype.Component;
import se.peltarion.tictactoe.model.service.Move;
import se.peltarion.db.tables.records.MoveRecord;

@Component
public class MoveMapper implements RecordMapper<MoveRecord, Move> {
    @Override
    public Move map(MoveRecord moveRecord) {
        return Move.Builder
            .uuid(moveRecord.getUuid())
            .row(moveRecord.getRow())
            .col(moveRecord.getCol())
            .gameUuid(moveRecord.getGameUuid())
            .playerUuid(moveRecord.getPlayerUuid())
            .created(moveRecord.getCreated())
            .build();
    }
}