package se.peltarion.tictactoe.service.recordmapper;

import org.jooq.RecordMapper;
import org.springframework.stereotype.Component;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.db.tables.records.GameRecord;

@Component
public class GameMapper implements RecordMapper<GameRecord, Game> {
    @Override
    public Game map(GameRecord gameRecord) {
        return Game.Builder
            .uuid(gameRecord.getUuid())
            .playerOneUuid(gameRecord.getPlayerOneUuid())
            .playerOneSymbol(gameRecord.getPlayerOneSymbol())
            .playerTwoUuid(gameRecord.getPlayerTwoUuid())
            .playerTwoSymbol(gameRecord.getPlayerTwoSymbol())
            .nextMovePlayerUuid(gameRecord.getNextMovePlayerUuid())
            .status(gameRecord.getStatus())
            .winner(gameRecord.getWinner())
            .created(gameRecord.getCreated())
            .build();
    }
}