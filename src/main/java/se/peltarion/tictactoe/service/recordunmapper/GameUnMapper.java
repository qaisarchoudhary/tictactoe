package se.peltarion.tictactoe.service.recordunmapper;

import static se.peltarion.db.Tables.GAME;
import org.jooq.RecordUnmapper;
import org.jooq.exception.MappingException;
import org.springframework.stereotype.Component;
import se.peltarion.tictactoe.model.service.Game;
import se.peltarion.db.tables.records.GameRecord;

@Component
public class GameUnMapper implements RecordUnmapper<Game, GameRecord> {

  @Override
  public GameRecord unmap(Game game) throws MappingException {
    GameRecord gameRecord = new GameRecord();
    gameRecord.set(GAME.UUID, game.getUuid());
    gameRecord.set(GAME.PLAYER_ONE_UUID, game.getPlayerOneUuid());
    gameRecord.set(GAME.PLAYER_ONE_SYMBOL, game.getPlayerOneSymbol());
    gameRecord.set(GAME.PLAYER_TWO_UUID, game.getPlayerTwoUuid());
    gameRecord.set(GAME.PLAYER_TWO_SYMBOL, game.getPlayerTwoSymbol());
    gameRecord.set(GAME.NEXT_MOVE_PLAYER_UUID ,game.getNextMovePlayerUuid());
    gameRecord.set(GAME.STATUS, game.getStatus());
    gameRecord.set(GAME.WINNER, game.getWinner());
    gameRecord.set(GAME.CREATED, game.getCreated());
    return gameRecord;
  }
}
