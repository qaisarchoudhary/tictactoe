package se.peltarion.tictactoe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class GameNotAvailableException extends RuntimeException {
    private static String message = "Game not available exception";

    public GameNotAvailableException() {
        super(message);
    }

    public GameNotAvailableException(String message) {
        super(message);
    }

//    @Override
//    public synchronized Throwable fillInStackTrace() {
//        return this;
//    }
}
