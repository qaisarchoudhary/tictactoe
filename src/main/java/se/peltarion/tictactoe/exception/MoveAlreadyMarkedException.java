package se.peltarion.tictactoe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class MoveAlreadyMarkedException extends RuntimeException {
    private static String message = "Move already used exception";

    public MoveAlreadyMarkedException() {
        super(message);
    }

    public MoveAlreadyMarkedException(String message) {
        super(message);
    }

}
