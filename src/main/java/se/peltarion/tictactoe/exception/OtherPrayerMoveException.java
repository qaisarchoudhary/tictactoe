package se.peltarion.tictactoe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class OtherPrayerMoveException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private static String message = "Other player's move exception";

    public OtherPrayerMoveException() {
        super(message);
    }

    public OtherPrayerMoveException(String message) {
        super(message);
    }

}
