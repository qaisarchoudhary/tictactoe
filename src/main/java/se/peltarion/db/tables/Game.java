/*
 * This file is generated by jOOQ.
 */
package se.peltarion.db.tables;


import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row9;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

import se.peltarion.db.Keys;
import se.peltarion.db.Tictactoe;
import se.peltarion.db.tables.records.GameRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Game extends TableImpl<GameRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>tictactoe.game</code>
     */
    public static final Game GAME = new Game();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<GameRecord> getRecordType() {
        return GameRecord.class;
    }

    /**
     * The column <code>tictactoe.game.uuid</code>.
     */
    public final TableField<GameRecord, java.util.UUID> UUID = createField(DSL.name("uuid"), SQLDataType.UUID.nullable(false).defaultValue(DSL.field("tictactoe.uuid_generate_v4()", SQLDataType.UUID)), this, "");

    /**
     * The column <code>tictactoe.game.player_one_uuid</code>.
     */
    public final TableField<GameRecord, java.util.UUID> PLAYER_ONE_UUID = createField(DSL.name("player_one_uuid"), SQLDataType.UUID, this, "");

    /**
     * The column <code>tictactoe.game.player_one_symbol</code>.
     */
    public final TableField<GameRecord, String> PLAYER_ONE_SYMBOL = createField(DSL.name("player_one_symbol"), SQLDataType.VARCHAR(200), this, "");

    /**
     * The column <code>tictactoe.game.player_two_uuid</code>.
     */
    public final TableField<GameRecord, java.util.UUID> PLAYER_TWO_UUID = createField(DSL.name("player_two_uuid"), SQLDataType.UUID, this, "");

    /**
     * The column <code>tictactoe.game.player_two_symbol</code>.
     */
    public final TableField<GameRecord, String> PLAYER_TWO_SYMBOL = createField(DSL.name("player_two_symbol"), SQLDataType.VARCHAR(200), this, "");

    /**
     * The column <code>tictactoe.game.next_move_player_uuid</code>.
     */
    public final TableField<GameRecord, java.util.UUID> NEXT_MOVE_PLAYER_UUID = createField(DSL.name("next_move_player_uuid"), SQLDataType.UUID, this, "");

    /**
     * The column <code>tictactoe.game.status</code>.
     */
    public final TableField<GameRecord, String> STATUS = createField(DSL.name("status"), SQLDataType.VARCHAR(200).nullable(false), this, "");

    /**
     * The column <code>tictactoe.game.winner</code>.
     */
    public final TableField<GameRecord, java.util.UUID> WINNER = createField(DSL.name("winner"), SQLDataType.UUID, this, "");

    /**
     * The column <code>tictactoe.game.created</code>.
     */
    public final TableField<GameRecord, OffsetDateTime> CREATED = createField(DSL.name("created"), SQLDataType.TIMESTAMPWITHTIMEZONE(6), this, "");

    private Game(Name alias, Table<GameRecord> aliased) {
        this(alias, aliased, null);
    }

    private Game(Name alias, Table<GameRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>tictactoe.game</code> table reference
     */
    public Game(String alias) {
        this(DSL.name(alias), GAME);
    }

    /**
     * Create an aliased <code>tictactoe.game</code> table reference
     */
    public Game(Name alias) {
        this(alias, GAME);
    }

    /**
     * Create a <code>tictactoe.game</code> table reference
     */
    public Game() {
        this(DSL.name("game"), null);
    }

    public <O extends Record> Game(Table<O> child, ForeignKey<O, GameRecord> key) {
        super(child, key, GAME);
    }

    @Override
    public Schema getSchema() {
        return Tictactoe.TICTACTOE;
    }

    @Override
    public UniqueKey<GameRecord> getPrimaryKey() {
        return Keys.GAME_PKEY;
    }

    @Override
    public List<UniqueKey<GameRecord>> getKeys() {
        return Arrays.<UniqueKey<GameRecord>>asList(Keys.GAME_PKEY);
    }

    @Override
    public Game as(String alias) {
        return new Game(DSL.name(alias), this);
    }

    @Override
    public Game as(Name alias) {
        return new Game(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Game rename(String name) {
        return new Game(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Game rename(Name name) {
        return new Game(name, null);
    }

    // -------------------------------------------------------------------------
    // Row9 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row9<java.util.UUID, java.util.UUID, String, java.util.UUID, String, java.util.UUID, String, java.util.UUID, OffsetDateTime> fieldsRow() {
        return (Row9) super.fieldsRow();
    }
}
