/*
 * This file is generated by jOOQ.
 */
package se.peltarion.db.tables.daos;


import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;

import se.peltarion.db.tables.Move;
import se.peltarion.db.tables.records.MoveRecord;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class MoveDao extends DAOImpl<MoveRecord, se.peltarion.db.tables.pojos.Move, UUID> {

    /**
     * Create a new MoveDao without any configuration
     */
    public MoveDao() {
        super(Move.MOVE, se.peltarion.db.tables.pojos.Move.class);
    }

    /**
     * Create a new MoveDao with an attached configuration
     */
    public MoveDao(Configuration configuration) {
        super(Move.MOVE, se.peltarion.db.tables.pojos.Move.class, configuration);
    }

    @Override
    public UUID getId(se.peltarion.db.tables.pojos.Move object) {
        return object.getUuid();
    }

    /**
     * Fetch records that have <code>uuid BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchRangeOfUuid(UUID lowerInclusive, UUID upperInclusive) {
        return fetchRange(Move.MOVE.UUID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>uuid IN (values)</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchByUuid(UUID... values) {
        return fetch(Move.MOVE.UUID, values);
    }

    /**
     * Fetch a unique record that has <code>uuid = value</code>
     */
    public se.peltarion.db.tables.pojos.Move fetchOneByUuid(UUID value) {
        return fetchOne(Move.MOVE.UUID, value);
    }

    /**
     * Fetch records that have <code>row BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchRangeOfRow(Integer lowerInclusive, Integer upperInclusive) {
        return fetchRange(Move.MOVE.ROW, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>row IN (values)</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchByRow(Integer... values) {
        return fetch(Move.MOVE.ROW, values);
    }

    /**
     * Fetch records that have <code>col BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchRangeOfCol(Integer lowerInclusive, Integer upperInclusive) {
        return fetchRange(Move.MOVE.COL, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>col IN (values)</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchByCol(Integer... values) {
        return fetch(Move.MOVE.COL, values);
    }

    /**
     * Fetch records that have <code>game_uuid BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchRangeOfGameUuid(UUID lowerInclusive, UUID upperInclusive) {
        return fetchRange(Move.MOVE.GAME_UUID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>game_uuid IN (values)</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchByGameUuid(UUID... values) {
        return fetch(Move.MOVE.GAME_UUID, values);
    }

    /**
     * Fetch records that have <code>player_uuid BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchRangeOfPlayerUuid(UUID lowerInclusive, UUID upperInclusive) {
        return fetchRange(Move.MOVE.PLAYER_UUID, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>player_uuid IN (values)</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchByPlayerUuid(UUID... values) {
        return fetch(Move.MOVE.PLAYER_UUID, values);
    }

    /**
     * Fetch records that have <code>created BETWEEN lowerInclusive AND upperInclusive</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchRangeOfCreated(OffsetDateTime lowerInclusive, OffsetDateTime upperInclusive) {
        return fetchRange(Move.MOVE.CREATED, lowerInclusive, upperInclusive);
    }

    /**
     * Fetch records that have <code>created IN (values)</code>
     */
    public List<se.peltarion.db.tables.pojos.Move> fetchByCreated(OffsetDateTime... values) {
        return fetch(Move.MOVE.CREATED, values);
    }
}
