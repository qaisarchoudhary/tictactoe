
docker-compose -f postgres-deploy.yaml up -d

#sleep 5

cd ../../../

echo 'starting build...'

echo 'Doing database migrations'
./gradlew flywayMigrate
echo 'database migrations done!'
echo 'Now generating JOOQ auto-generated code'
./gradlew generateJooqSourceCode
echo 'Done with JOOQ'
echo 'Now building the service'
./gradlew build
echo 'Build completed successfully...'


pwd

cp build/libs/tictactoe-0.0.1-SNAPSHOT.jar src/main/docker/

cd src/main/docker


docker build --build-arg HOSTNAME="192.168.1.147" --build-arg PORT=5432 -t tictactoe-service-image .

docker-compose -f tictactoe-deploy.yaml up -d

rm tictactoe-0.0.1-SNAPSHOT.jar

echo 'Deployment completed...'

